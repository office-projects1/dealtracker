package uk.co.chandra.dealtracker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DealtrackerApplication {

	public static void main(String[] args) {
		SpringApplication.run(DealtrackerApplication.class, args);
	}

}
